package de.bensimple.opf.main;

import de.bensimple.opf.commands.Command_opfood;
import de.bensimple.opf.listeners.Player_join;
import de.bensimple.opf.listeners.Player_menu;
import de.bensimple.opf.listeners.Player_use;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        System.out.println("-----------");
        System.out.println("");
        System.out.println("Operation Food V 0.5 erfolgreich geladen");
        System.out.println("");
        System.out.println("-----------");


        getCommand("opfood").setExecutor(new Command_opfood());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new Player_join(), this);
        pm.registerEvents(new Player_use(), this);
        pm.registerEvents(new Player_menu(), this);

    }




    @Override
    public void onDisable() {


    }
}
