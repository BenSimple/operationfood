package de.bensimple.opf.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class Player_join implements Listener {

    @EventHandler
    public ItemStack join(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        p.getInventory().setItem(4, getNetherStar());
        return getNetherStar();

    }

    public static ItemStack getNetherStar() {

        ItemStack nether_star = new ItemStack(Material.NETHER_STAR);
        ItemMeta meta = nether_star.getItemMeta();
        meta.setDisplayName("§2§lMenu");
        nether_star.setItemMeta(meta);
        return nether_star;

    }
}

