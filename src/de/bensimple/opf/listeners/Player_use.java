package de.bensimple.opf.listeners;

import de.bensimple.opf.utils.ChestSlot;
import de.bensimple.opf.utils.Item;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class Player_use implements Listener {

    @EventHandler
    public void use(PlayerInteractEvent event) {

        Player p = event.getPlayer();

        if (p.getInventory().getItemInMainHand() != null) {
            if (p.getInventory().getItemInMainHand().isSimilar(Player_join.getNetherStar())){
                if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK);
                event.setCancelled(true);
                p.openInventory(getInventory());

            }


        }


    }

    public static String inv_name = "§3§oMENU by BenSimple"; //Inventarname, der String davon

    public static Inventory getInventory() {

        Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST, inv_name);
        inv.setItem(ChestSlot.getSlot(2, 2), getOpfood().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 1), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 2), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 3), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 4), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 5), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 6), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 7), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 8), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(1, 9), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 1), glasspanel().getItemStack());
        //inv.setItem(ChestSlot.getSlot(2, 2), glasspanel().getItemStack()); Hier ist das Item
        inv.setItem(ChestSlot.getSlot(2, 3), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 4), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 5), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 6), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 7), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 8), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(2, 9), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 1), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 2), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 3), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 4), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 5), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 6), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 7), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 8), glasspanel().getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 9), redstone_torch().getItemStack());

        return inv;
    }

    public static Item getOpfood() {
        Item opfood = new Item(Material.COOKED_BEEF, 1)
                .setName("§3§lSpezial-Steak")
                .addLoreLine("§2>Füllt §4Leben §2und Hunger wieder auf")
                .addLoreLine("§2>Das Essen kommt aus §klululluu");
        return opfood;
    }

    public static Item glasspanel() {
        Item glasspanel = new Item(Material.GRAY_STAINED_GLASS_PANE, 1)
                .setName("");
        return glasspanel;

    }

    public static Item redstone_torch() {
        Item redstone_torch = new Item(Material.REDSTONE_TORCH, 1)
                .setName("§cWusstest du schon ?")
                .addLoreLine("§2 Du kannst mit /opfood den selben Effekt erzielen");
        return redstone_torch;
    }
}


